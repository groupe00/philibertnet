*** Settings ***
Documentation    Chercher un jeu et consulter la description
Library    SeleniumLibrary
Resource    PhilibertRechercheJeuRessource.resource

*** Test Cases ***
SearchGame Magic
  [Setup]    Test Setup
    Given Je suis sur la page d'accueil
    When Je souhaite rechercher un jeu    ${JDD1.Jeu}
    And J'accède à la page des résultats de la recherche    ${JDD1.Jeu}
    And Je clique sur le résultat correspondant    ${JDD1.Jeu}
    Then J'accède à la page dédiée au jeu    ${JDD1.Title}    ${JDD1.Jeu}
    And Je peux consulter la description
  [Teardown]    Test TearDown

SearchGame Marvel
  [Setup]    Test Setup
    Given Je suis sur la page d'accueil
    When Je souhaite rechercher un jeu    ${JDD2.Jeu}
    And J'accède à la page des résultats de la recherche    ${JDD2.Jeu}
    And Je clique sur le résultat correspondant    ${JDD2.Jeu}
    Then J'accède à la page dédiée au jeu    ${JDD2.Title}    ${JDD2.Jeu}
    And Je peux consulter la description
  [Teardown]    Test TearDown