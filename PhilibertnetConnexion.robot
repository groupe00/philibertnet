*** Settings ***
Documentation    Se connecter à Philibertnet.com
Library    SeleniumLibrary
Resource    PhilibertnetConnexionRessource.resource

*** Test Cases ***
Se connecter
     [Setup]    Test Setup   
    Given Je suis sur la page d'accueil
    When Je souhaite me connecter
    And J'accède à la page d'authentification
    And Je remplis le formulaire de login    t3st.test@outlook.fr    T3st123tes7!
    And Je clique sur Identifiez-vous 
    Then J'accède à mon compte
    And Un message de bienvenue apparait
    And Le bouton Déconnexion apparait
    [Teardown]    Test TearDown